class Book < ActiveRecord::Base
  include AlgoliaSearch

  after_commit :index!, on: :update
  validates :isbn, length: {maximum: 13, minimum: 13}, format: { with: /^[0-9]+$/, on: :create, on: :update, multiline: true}

  algoliasearch per_environment: true, if: :available? do
    attribute :title, :isbn, :author, :genre
  end

  private
  def available?
    availabilty > 0
  end

end
