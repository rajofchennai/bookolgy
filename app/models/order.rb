class Order < ActiveRecord::Base
  PICKUP = 0
  DROP   = 1

  def book
    Book.find_by isbn: self.isbn
  end

  def user
    User.find_by id: self.user_id
  end

  def self.create_or_find_order options
    user_id = options[:user_id]
    isbn = options[:isbn]
    operation = options[:operation]
    resp = {}
    if user_id && isbn && operation
      old_order = Order.find_by user_id: user_id, isbn: isbn, status: "open"
      if old_order
        resp[:status] = FAILURE
        resp[:error]  = "Duplicate order"
      else
        order = Order.create!(options)
        resp[:status] = SUCCESS
        resp[:order_id] = order.id
      end
      resp
    end
  end
end
