class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # TODO: remove this after fixing ajax
  skip_before_action :verify_authenticity_token

  before_filter :validate_params

  def current_user
    Rails.logger.error session[:user_id]
    login_from_session
    unless @user
      render "users/login"
    end
  end

  def login_from_session
    @user = User.find_by_id(session[:user_id]) if session[:user_id]
  end

  def validate_params
    if params[:isbn]
      isbn = params[:isbn]
      unless isbn.length == 13 && isbn.match(/^[0-9]+$/)
        head :bad_request
      end
    end
  end
end
