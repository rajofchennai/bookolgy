class SearchController < ApplicationController
  # before_filter :current_user

  before_filter :login_from_session

  def index
    # render default home page
  end

  def query
    @query = params[:query]
    @books = Book.search(@query)
    # render list of search results
  end
end
