class OrdersController < ApplicationController
  include BooksHelper
  include OrdersHelper
  before_filter :is_admin?, only: [:satisfy, :cancel]
  before_filter :validate_isbn, only: :place_order
  before_filter :current_user
  before_filter :find_or_create_books, only: :place

  def satisfy
    order_ids = params[:order_ids].split(",")
    orders = Order.where(id: order_ids)
    orders.each do |order|
      if order.operation == Order::PICKUP
        book = order.book
        user = order.user
        book.availabilty += 1
        book.save!
        user.credits += order.credits
        user.save!
      end
      order.status = "closed"
      order.save!
    end
    render nothing: true
  end

  def cancel
    order = Order.find(id: order_id)
    if order.operation == Order::DROP
      book = order.book
      user = order.user
      book.availabilty += 1
      book.save!
      user.credits += order.credits
      user.save!
    end
    order.status = "canceled"
    order.save!
  end

  def place
    if params[:operation] == "share"
      @book = PotentialBook.create(isbn: params[:isbn], title: params[:title], price: params[:price], credits: params[:credits]) unless @book
      @resp = Order.create_or_find_order create_order_params(params)
    elsif params[:operation] == "buy"
      if check_credit_sufficiency
        if @book.availabilty > 0
          @resp = Order.create_or_find_order create_order_params(params)
          if @resp[:status] == SUCCESS
            @user.credits -= @book.credits
            @user.save!
            @book.availabilty -= 1
            @book.save!
          end
        else
          @resp = { status: FAILURE, error: "Book Not available" }
        end
      else
        @resp = { status: FAILURE, error: "Insufficient credits" }
      end
    end
  end

  def myorders
    @all_orders = Order.where(user_id: @user.id)
  end

  def place_order
  end

  def is_admin?
    User.last  #relook
  end
  private

  def create_order_params param
    if param[:operation] == "share"
      operation = Order::PICKUP
    elsif param[:operation] == "buy"
      operation = Order::DROP
    end
    { user_id: @user.id, isbn: @book.isbn, operation: operation, name: param[:name], pincode: param[:pincode], address: param[:address], city: param[:city], state: param[:state], country: param[:country], phone: param[:phone], price: @book.price || param[:price], credits: @book.credits || param[:credits]}
  end

end
