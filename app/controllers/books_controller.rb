class BooksController < ApplicationController
  include BooksHelper
  include OrdersHelper
  before_filter :validate_isbn, only: :buy
  before_filter :find_or_create_books, only: [:share, :share_get_details, :add]
  before_filter :current_user

  def buy
  end

  def place_order
  end

  def share
  end

  def share_intro
  end

  def add
    create_book_from_amazon unless @book
    @book.availabilty += 1
    @book.save!
    render nothing: true
  end

  def share_get_details
    create_book_from_amazon unless @book
  end

end
