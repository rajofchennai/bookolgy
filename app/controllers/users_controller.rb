class UsersController < ApplicationController
  def oauth2
    Rails.logger.error params.inspect
    @user = User.find_by gplus: params[:id]
    Rails.logger.error @user.inspect
    unless @user
      @user = User.create!(gplus: params[:id], access_token: params[:access_token], name: params[:name])
    end
    Rails.logger.error @user.id
    session[:user_id] = @user.id
    render nothing: true
  end

  def login
  end
end
