module OrdersHelper
  def check_credit_sufficiency
    @user.credits > @book.credits
  end
end
