module BooksHelper
  def validate_isbn
    @book = Book.find_by_isbn(params[:isbn])
    unless @book
      # render invalid book page
    end
  end

  def find_or_create_books
    @book = Book.find_by_isbn(params[:isbn])
  end

  def create_book_from_amazon
    item = Amazon.search_by_isbn params[:isbn]
      if item
        title = item.item_attributes.title
        if item.offer_summary.lowest_new_price
          price = item.offer_summary.lowest_new_price.amount.to_i*0.01
          credits = price*0.7*0.01
        else
          price = nil
          credits = nil
        end
        author = item.item_attributes.author.to_s
        @book = Book.create(isbn: params[:isbn], title: title, credits: credits, price: price, availabilty: 0, author: author, genre: "") # TODO: price comes in paise # hard coding credits offered
      end
  end
end
