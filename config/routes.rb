Bookolgy::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root to: redirect('/buy')

  match "/buy", to: 'search#index', via: :get
  match "/buy/search", to: 'search#query', via: :get
  match "/buy", to: 'books#buy', via: :post
  match "/:operation/confirm", to: 'orders#place', via: :post, constraints: {operation: /buy|share/}

  match "/share", to: 'books#share_intro', via: :get
  match "/share/search", to: 'books#share_get_details', via: :get
  match "/share", to: 'books#share', via: :post

  match "/credits/buy", to: 'credits#buy_intro', via: :get
  match "/credits/buy", to: 'credits#buy', via: :post

#  match "/orders/satisfy", to: 'orders#satisfy', via: :post
  match "/orders/cancel", to: 'orders#cancel', via: :post
  match "/orders"  => 'orders#myorders', :via => :get

  match "/oauth2callback", to: 'users#oauth2', via: :post

  match "/login", to: 'users#login', via: :get


  match "/admin", to: "admin#index", via: :get
  match "/admin/books/add", to:  "books#add", via: :post
  match "/admin/orders/satisfy", to: "orders#satisfy", via: :post
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member droot :to => 'home#index'
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
