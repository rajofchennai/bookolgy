# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140506185752) do

  create_table "books", force: true do |t|
    t.string   "isbn"
    t.text     "title"
    t.float    "credits",     default: 0.0
    t.integer  "availabilty", default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "price",       default: 0
    t.string   "author"
    t.string   "genre"
  end

  add_index "books", ["isbn"], name: "index_books_on_isbn", unique: true, using: :btree

  create_table "orders", force: true do |t|
    t.integer  "user_id"
    t.string   "isbn"
    t.string   "address"
    t.integer  "operation"
    t.string   "pincode"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "phone"
    t.string   "name"
    t.string   "status",     default: "open"
    t.float    "credits",    default: 0.0
    t.float    "price",      default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "orders", ["isbn"], name: "index_orders_on_isbn", using: :btree
  add_index "orders", ["user_id", "isbn"], name: "index_orders_on_user_id_and_isbn", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "potential_books", force: true do |t|
    t.string   "isbn"
    t.text     "title"
    t.integer  "price"
    t.float    "credits"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string "name"
    t.float  "credits",      default: 0.0
    t.string "gplus"
    t.string "access_token"
  end

  add_index "users", ["gplus"], name: "index_users_on_gplus", unique: true, using: :btree

end
