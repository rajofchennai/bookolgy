class Books < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :isbn
      t.text   :title
      t.float  :credits
      t.integer :availabilty
      t.timestamps
    end
  end
end
