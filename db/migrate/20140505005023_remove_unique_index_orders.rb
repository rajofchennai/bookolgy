class RemoveUniqueIndexOrders < ActiveRecord::Migration
  def change
    remove_index :orders, [:user_id, :isbn]  # removing unique index
    add_index :orders, [:user_id, :isbn] # adding unique index
  end
end
