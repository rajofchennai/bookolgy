class AddDefaulttoUserCreditBookCredits < ActiveRecord::Migration
  def change
    change_column :books, :credits, :float, :default => 0.0
    change_column :books, :availabilty, :integer, :default => 0
    change_column :users, :credits, :float, :default => 0.0
    add_index :books, :isbn, :unique => true
    add_index :orders, :user_id
    add_index :orders, :isbn
    add_index :orders, [:user_id, :isbn], :unique => true
    add_index :users, :gplus, :unique => true
  end
end
