class AddColumnOrder < ActiveRecord::Migration
  def change
    add_column :orders, :pincode, :string
    add_column :orders, :city, :string
    add_column :orders, :state, :string
    add_column :orders, :country, :string
    add_column :orders, :phone, :string
  end
end
