class Users < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :name
      t.float  :credits
    end
  end
end
