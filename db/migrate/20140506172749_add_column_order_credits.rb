class AddColumnOrderCredits < ActiveRecord::Migration
  def change
    add_column :orders, :credits, :float, default: 0.0
  end
end
