class AddColumnsToBooks < ActiveRecord::Migration
  def change
    add_column :books, :author, :string
    add_column :books, :genre, :string
  end
end
