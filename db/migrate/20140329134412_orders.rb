class Orders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.string  :isbn
      t.string  :address
      t.integer :operation # 0 for pickup 1 for drop
    end
  end
end
