class CreatePotentialBooks < ActiveRecord::Migration
  def change
    create_table :potential_books do |t|
      t.string :isbn
      t.text   :title
      t.integer  :price
      t.float    :credits
      t.timestamps
    end
  end
end
